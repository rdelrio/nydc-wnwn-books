import { defineCustomElement } from 'vue'
import Section from './Section.ce.vue'

const SectionElement = defineCustomElement(Section)
window?.customElements.define('nydc-books', SectionElement)