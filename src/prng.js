// https://github.com/bryc/code/blob/master/jshash/PRNGs.md

// MurmurHash3 implementation
export const xmur3 = str => {
  for (var i = 0, h = 1779033703 ^ str.length; i < str.length; i++)
    h = Math.imul(h ^ str.charCodeAt(i), 3432918353),
    h = h << 13 | h >>> 19;

  return () => {
    h = Math.imul(h ^ h >>> 16, 2246822507),
    h = Math.imul(h ^ h >>> 13, 3266489909);
    return (h ^= h >>> 16) >>> 0;
  }
}

// Mulberry32
export const mulberry32 = (a = 0) => {
  return () => {
    a |= 0; a = a + 0x6D2B79F5 | 0;
    let t = Math.imul(a ^ a >>> 15, 1 | a);
    t = t + Math.imul(t ^ t >>> 7, 61 | t) ^ t;
    return ((t ^ t >>> 14) >>> 0) / 4294967296;
  }
}

export const clamp = (min = 0, max = 1, round = false) => {
  const r = max - min
  return round
    ? (n = 0) => Math.round(min + n * r)
    : (n = 0) => min + n * r
}


export const seed = xmur3;
export const rand = mulberry32;
export const prng = str => mulberry32(xmur3(str)())

window.seed = seed;
window.rand = rand;
window.prng = prng;

export default prng;