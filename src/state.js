// src/composables/state.js
import { ref, readonly, reactive } from 'vue';

export function useState(initialState) {
  const state = reactive(initialState);
  const setState = (newState) => {
    state.value = newState;
  };
  
  return [readonly(state), setState];
}

export const globalState = useState({
  debug: true,
  wheel: ref([0,0]),
  pointer: ref([0,0]),
  element: ref([0,0]),
  origin: ref([0,0]),
})

const store = {
  debug: true,
}