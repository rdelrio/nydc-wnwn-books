## Usage

```html
<section style="--h: 100vw;" id="books">
  <nydc-books seed="A string for the prng" api-base="https://staging.nydc.etcetc">
    <!-- Section Preview Content -->
    <h1>We’ve gathered our favorite design books</h1>
    <p>orem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nisl quam, tincidunt at ligula ut, gravida fermentum lorem. Aliquam non neque a quam porta vehicula vel vitae justo. Cras dapibus.</p>
  </nydc-books>
</section>
<!-- ... -->
<script src="/path/to/wnwn-books.umd.js">
```
Note you can either use the UMD version in a <scirpt> tag or import with the theme's webpack setup

## Data Loading
Preload content statically by printing escaped JSON in the `data` attribute *or* do it at runtime via JS:
```js
const customEl = document.querySelector('nydc-books')
customEl.data = data
```

### API Assumptions
The schema for data assumes you're using the WP-JSON api. A payload for the data object looks like this:

```js
{
  "thumbs": [
    // Array of attachment objects resembling payload of /wp-json/wp/v2/attachments
    {
      "id": 0, // id of the attachment
      "book": 0, // id of the associated book (see below)
      "media_details": {
        "sizes": {
          // image size array
          "full": {
            "width": 250,
            "height": 550,
            "source_url": "https://placehold.co/250x550"
          }
        }
      }
    },
    // { ... }
  ]
}
```

From there we can fetch book data from a Books post type which should have a payload like this:
```js
// fetched from /wp-json/wp/v2/books/*
{
  "id": 1000,
  "title": {
      "rendered": "Vivamus vestibulum ntulla nec ante"
  },
  "content": { "rendered": "lorem" },
  "excerpt": { "rendered": "lorem" },
  "acf": {
    "author": { "name": "asdf" },
    "purchase_link": { "href": "https://alskdfjl" },
    "gallery": [ /* array of attachment objects maybe? */ ],
  }
}
```

Optionally if you want to go full static you can include all Book post type data as a keyed object-array in the data payload like this:
```js
{
  "thumbs": [ /*...*/ ]
  "books": {
    "1000": { "id": "1000", /*...*/ },
  }
}
```