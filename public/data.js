const exampleAttachment = {
  "id": 401,
  "date": "2016-06-03T17:29:09",
  "date_gmt": "2016-06-03T17:29:09",
  "guid": {
    "rendered": "http://example.com/wp-content/uploads/my-image-name.png"
  },
  "modified": "2016-06-03T17:29:09",
  "modified_gmt": "2016-06-03T17:29:09",
  "slug": "my-image-name",
  "type": "attachment",
  "link": "http://example.com/my-post-url",
  "title": {
    "rendered": "my-image-name"
  },
  "author": 1,
  "comment_status": "open",
  "ping_status": "closed",
  "alt_text": "",
  "caption": "",
  "description": "",
  "media_type": "image",
  "mime_type": "image/png",
  "media_details": {
    "width": 1600,
    "height": 2560,
    "file": "my-image-name.png",
    "sizes": {
      "thumbnail": {
        "file": "my-image-name-150x150.png",
        "width": 150,
        "height": 150,
        "mime_type": "image/png",
        "source_url": "https://placehold.co/150x150"
      },
      "medium": {
        "file": "my-image-name-300x136.png",
        "width": 300,
        "height": 136,
        "mime_type": "image/png",
        "source_url": "https://placehold.co/300x136"
      },
      "one-paze-port-thumb": {
        "file": "my-image-name-363x250.png",
        "width": 363,
        "height": 250,
        "mime_type": "image/png",
        "source_url": "https://placehold.co/363x250"
      },
      "one-paze-blog-thumb": {
        "file": "my-image-name-270x127.png",
        "width": 270,
        "height": 127,
        "mime_type": "image/png",
        "source_url": "https://placehold.co/270x127"
      },
      "one-paze-team-thumb": {
        "file": "my-image-name-175x175.png",
        "width": 175,
        "height": 175,
        "mime_type": "image/png",
        "source_url": "https://placehold.co/175x175"
      },
      "one-paze-testimonial-thumb": {
        "file": "my-image-name-79x79.png",
        "width": 79,
        "height": 79,
        "mime_type": "image/png",
        "source_url": "https://placehold.co/79x79"
      },
      "one-paze-blog-medium-image": {
        "file": "my-image-name-380x250.png",
        "width": 380,
        "height": 250,
        "mime_type": "image/png",
        "source_url": "https://placehold.co/380x250"
      },
      "full": {
        "file": "my-image-name.png",
        "width": 1600,
        "height": 2560,
        "mime_type": "image/png",
        "source_url": "https://placehold.co/1600x2560"
      }
    },
    "image_meta": {
      "aperture": "0",
      "credit": "",
      "camera": "",
      "caption": "",
      "created_timestamp": "0",
      "copyright": "",
      "focal_length": "0",
      "iso": "0",
      "shutter_speed": "0",
      "title": "",
      "orientation": "0",
      "keywords": [ ]
    }
  },
  "post": 284,
  "source_url": "http://example.com/wp-content/uploads/my-image-name.png",
  "_links": {
    "self": [
      {
        "href": "http://example.com/wp-json/wp/v2/media/401"
      }
    ],
    "collection": [
      {
        "href": "http://example.com/wp-json/wp/v2/media"
      }
    ],
    "about": [
      {
        "href": "http://example.com/wp-json/wp/v2/types/attachment"
      }
    ],
    "author": [
      {
        "embeddable": true,
        "href": "http://example.com/wp-json/wp/v2/users/1"
      }
    ],
    "replies": [
      {
        "embeddable": true,
        "href": "http://example.com/wp-json/wp/v2/comments?post=401"
      }
    ]
  } 
}

const exampleBook = {
  "id": 1000,
  "date": "2017-07-21T10:30:34",
  "date_gmt": "2017-07-21T17:30:34",
  "guid": {
    "rendered": "https://www.example.com/?p=157538"
  },
  "modified": "2017-07-23T21:56:35",
  "modified_gmt": "2017-07-24T04:56:35",
  "type": "book",
  "slug": "asdf",
  "status": "publish",
  "type": "post",
  "link": "https://www.example.com/asdf/",
  "title": {
      "rendered": "Vivamus vestibulum ntulla nec ante"
  },
  "content": { "rendered": "lorem" },
  "excerpt": { "rendered": "lorem" },
  "author": 72546,
  "featured_media": 157542,
  "featured_image": exampleAttachment,
  "acf": {
    "images": [
      exampleAttachment,
      exampleAttachment,
    ]
  },
  "comment_status": "open",
  "ping_status": "closed",
  "sticky": false,
  "template": "",
  "format": "standard",
  "meta": [],
  "categories": [
      6132
  ],
  "tags": [
      1798,
      6298
  ],
}

window.data = {
  books: {
    "2": exampleBook,
    "3": exampleBook,
    "4": exampleBook,
    "5": exampleBook,
    "6": exampleBook,
    "7": exampleBook,
    "8": exampleBook,
    "9": exampleBook,
  },
  thumbs: [
    {
      id: 2,
      book: 2,
      title: { rendered: 'Aliquam tincidunt mauris eu risus.' },
      image: exampleAttachment,
    },
    {
      id: 3,
      book: 3,
      title: { rendered: 'Vivamus vestibulum ntulla nec ante' },
      image: exampleAttachment,
    },
    {
      id: 4,
      book: 4,
      title: { rendered: 'Cras iaculis ultricies nulla' },
      image: exampleAttachment,
    },
    {
      id: 5,
      book: 5,
      title: { rendered: 'Praesent placerat' },
      image: exampleAttachment,
    },
    {
      id: 6,
      book: 6,
      title: { rendered: 'Aliquam tincidunt mauris eu risus.' },
      image: exampleAttachment,
    },
    {
      id: 7,
      book: 7,
      title: { rendered: 'Vivamus vestibulum ntulla nec ante' },
      image: exampleAttachment,
    },
    {
      id: 8,
      book: 8,
      title: { rendered: 'Cras iaculis ultricies nulla' },
      image: exampleAttachment,
    },
    {
      id: 9,
      book: 9,
      title: { rendered: 'Praesent placerat' },
      image: exampleAttachment,
    },
  ],
}